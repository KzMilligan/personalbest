# PersonalBest

Python script to update personal best times by comparing 
with data from [Power of 10](https://thepowerof10.info/).

Every entry in the input csv file where the 'Name' and 'Sport' cells 
contain valid data (in the case of 'Sport', this is anything except 
'control') is analysed. 
Power of 10 is queried with the athlete name and club; personal best times 
are retrieved from the table of results; then, the csv file is updated.

Along with the new csv file, a log file (`log.txt`) is generated. 
This will be written to the same location as the given output csv file.
The log contains the Participant ID, name and club for every athlete found 
and the url from which it obtained the PB data, followed by the original
and new PB data.
The end of the log file lists the ID, name and club for any athlete not
found on Power of 10.

New data will be added to the csv file in the format "Event Time", with 
multiple entries separated by semicolon.
Therefore, you may need to specify that semicolons should not be regarded as 
possible delimiters, when importing the output csv into a spreadsheet editor.

Tested on Windows 10 (with Anaconda/Miniconda) and Debian/Ubuntu.

## Requirements

Aside from Python 3, you will also need the Python 3 versions of the 
following modules:

* pandas
* BeautifulSoup
* requests
* urllib


## Usage

PersonalBest is a command line tool.

To display the full usage documentation:
```
python pb.py --help
```

PersonalBest requires an input csv file. If no output file name is 
provided, you will be asked if you wish to overwrite the input file.

If your csv file uses any delimiter other than a comma, please specify
it with `-d`. Please enclose the delimiter in quotation marks.

By default, an athlete's PB info will be overwritten by data retrieved 
from Power of 10; however, if you wish to leave this column untouched 
and instead add a new column for Power of 10 data, use the `-n` flag.

Finally, quiet mode can be enabled with `-q` and will prevent anything 
being written to stdout. This includes the file overwrite prompt that 
you would receive if no output file name is given.

## Example
```
python pb.py data.csv -o data_updated.csv
```
This will will overwrite data in the PB column and will output a new csv 
file called `data_updated.csv`, as well as the log file.

