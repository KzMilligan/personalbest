#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Read a csv file and update personal best data by comparing with Power of 10.

New data will be added to the csv file in the format "Event Time", with 
multiple entries separated by semicolon.

In addition to the csv, a log file (helpfully called log.txt) is also 
generated. 

Please see README for more information.
"""

import pandas as pd
from parse_po10 import get_athlete_PB
from progressbar import ProgressBar
import re
import os.path
import sys
import argparse


def update(file, outfile=None, sep=',', new_col=True, quiet=False):
    """ Read csv file and select relevant rows
    
        Parameters
        ----------
        file : str
            Path to input csv file
        outfile : str
            Path to write new csv file. If not provided, the input will be 
            overwritten.
        sep : str
            The csv delimiter. Default is comma.
        new_col : bool
            If True, updated PB info will be put in a new column. Otherwise,
            the current PB info will be overwritten.
        quiet : bool
            If quiet is True, nothing will be written to the console
    """
    
    # read csv into pandas DataFrame
    df = pd.read_csv(file, sep=sep)
    # keep log of all changes made
    log = ''
       
    # names of columns that we require
    idcol = 'Participant ID'
    nmcol = 'Participant name'
    clcol = 'Club at time of data collection, if relevant'
    pbcol = 'Event/Position/PBs/representative honours/team selections/etc.'
    
    # find rows where Participant name is given and Sport is not control
    indices = []
    for i in range(len(df)):
        row = df.iloc[i]
        if pd.notnull(row[nmcol]) and pd.notnull(row['Sport']):
            sport = str(row['Sport'])
            if not re.search(r'control', sport.lower()):
                indices.append(i)
        
    # make a new column, if requested, and initialise it with empty strings
    if new_col:
        pbcol_out = 'Event PB'
        # initialise the new column with a Series of empty strings
        values = pd.Series(['']*len(df))
        # insert the new column to the left of the existing PB column
        index = df.columns.get_loc(pbcol)-1
        df.insert(index, pbcol_out, values)
    else:
        pbcol_out = pbcol
    
    # count successful search/updates
    s_count = 0
    # keep Participant ID, name and club when data could not be found
    failed = []
    
    if not quiet:
        # warn user if given outfile will overwrite input
        if outfile is None:
            print('This will overwrite the input csv file. Are you sure you '
                  'wish to proceed?\n[Y/n]')
            proceed = input()
            if proceed.lower() == 'y' or not proceed:
                outfile = file
            elif proceed.lower() == 'n':
                sys.exit(0)
            else:
                raise Exception('Invalid input. Aborting.')
                sys.exit(1)
                
        # progress bar stuff
        count = 0
        size = len(indices)
        prog = ProgressBar(size)
    
    # analyse each selected line
    for idx in indices:

        # get data from current row
        p_id = df.iloc[idx][idcol]
        name = df.iloc[idx][nmcol]
        club = df.iloc[idx][clcol]
        pb_csv = df.iloc[idx][pbcol]
        
        # store all data in one handy dictionary
        # cast Participant ID to int, else pandas will change it to float
        data = {'ID':int(p_id), 'Name':name, 'Club':club, 'PB':pb_csv}
        
        # strip leading or trailing spaces (don't need to do this to ID data)
        for k, v in data.items():
            if k != 'ID':
                if pd.isnull(v):
                    data[k] = '' #None
                else:
                    data[k] = v.strip()
                
        try:
            # get this athlete's PBs from Power of 10
            url, pb_po10, s_link = get_athlete_PB(data['Name'], data['Club'])
            # format these as a nice string
            new_pb_csv = formatPB(pb_po10)
            
            # if a semicolon was given as the csv delimiter, the new data will
            # need to be quoted, to prevent the semicolons separating events 
            # being regarded as cell delimiters
            if sep == ';':
                new_pb_csv = '"{}"'.format(new_pb_csv)
            
            # this is the preferred way to set a value in the DataFrame, as
            # df.iloc[idx][pbcol] returns a view of the DataFrame
            df.at[idx, pbcol_out] = new_pb_csv
            
            # log the changes we've made
            log += '{}: {}, {}\n'.format(data['ID'], data['Name'], data['Club'])
            log += 'From\n  {}\nchose:\n'.format(s_link)
            log += '  {}\n'.format(url)
            log += 'Original PB data:\n  "{}"\n'.format(pb_csv)
            log += 'New PB data:\n   "{}"\n\n'.format(new_pb_csv)

            # increment success counter
            s_count += 1
            
        except ValueError:
            failed.append('{}: {}, {}\n'.format(data['ID'], data['Name'], 
                                                data['Club']))
        if not quiet:
            count += 1
            prog.progress(count)
            
    if not quiet:
        print('Updated {} successfully; failed to find {} athletes'
              .format(s_count, len(failed)))
    
    # because we've accessed the Participant ID column, which has missing data,
    # pandas has made the type of the column float, which means there are
    # trailing zeros
    # ID numbers shouldn't be floats, and NaN can't be cast to int, so format
    # the data as strings
    df[idcol] = df[idcol].apply(lambda x: '{:.0f}'.format(x) 
                                if pd.notnull(x) else '')
    # save the modified DataFrame
    df.to_csv(outfile, sep=sep, index=False)
    
    # log any athletes that were not found
    if len(failed) > 0:
        log += '\nAthletes not found on Power of 10:\n'
        log += ''.join(failed)
    log += '\n'
    
    # write log file to same location as new csv
    head, _ = os.path.split(outfile)
    with open('log.txt', 'w') as fileobj:
        pth = os.path.join(head, log)
        fileobj.write(pth)
        
                
def formatPB(pb_po10):
    """ Format the data from Po10 to be written to the csv
    
        Parameters
        ----------
        pb_po10 : dict
            Dictionary of Event:PB pairs, as returned by get_athlete_PB
            
        Returns 
        -------
        String to be added to the csv
    """
    
    # event and time will be space-separated
    # each event and time pair will be semicolon separated
    # any extraneous spaces will be removed
    out_lst = ['{} {}'.format(k.strip(),v.strip()) for k,v in pb_po10.items()]
    out = ';'.join(out_lst)

    
#    out = ''
#    
#    events = list(pb_po10.keys())
#    
#    # format all up to penultimate event with semicolon in between
#    for event in events[:-1]:
#        pb = pb_po10[event].strip()
#        event = event.strip()
#        out += '{} {}; '.format(event, pb)
#        
#    # put last event and PB in string
#    event = events[-1].strip()
#    pb = pb_po10[event].strip()
#    
#    out += '{} {}'.format(event, pb)
    
    return out


def compare(pb_po10, pb_csv):
    """ Compare the data from Po10 with the data already in the csv
    """
    pass

    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description=__doc__)
    
    parser.add_argument('input', 
                        help='CSV file of input data')
    parser.add_argument('-o', '--output', 
                        help='CSV file to write with updated data')
    parser.add_argument('-d', '--delimiter', 
                        help="CSV delimiter. Default is ','", 
                        default=',')
    parser.add_argument('-n', '--new_col',  action='store_true',
                        help="If new_col is specified, a new column for PB "
                        "data will be inserted before the current PB column. "
                        "Otherwise, the new data will overwrite the athlete's " 
                        "existing PB data.")
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='In quiet mode, nothing will be written to stdout')

    args = parser.parse_args()
    
    update(args.input, outfile=args.output, sep=args.delimiter, 
           new_col=args.new_col, quiet=args.quiet)
    