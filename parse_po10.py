#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions to get the link to an athlete's Power of 10 profile
"""

import bs4
import requests
import urllib.parse
import re


def get_athlete_PB(name, club=None):
    """ Get personal best data for an athlete
    
        Paramters
        ---------
        name : str
            Athlete's full name. If more than two names are given, the first
            and last will be taken as 'Firstname Surname'; others will be 
            ignored. If only one name is given, it will used as Surname.
        club : str, optional
            Athlete's club
            
        Returns
        -------
        The url from which it retrived the data and a dictionary of event:PB 
        pairs, along with the url of the search page.
    """
    
    cols = ['Event', 'PB']
    
    try:
        url, s_link = get_athelete_link(name, club)
    except ValueError as e:
        msg = "Could not find athlete profile for name={}".format(name)
        if club is not None:
            msg += ', club={}'.format(club)
        msg += '\nException: {}'.format(e)
        raise ValueError(msg)
        
    try:
        table = _get_subtable(url, 'div', 'cphBody_divBestPerformances', cols)
    except Exception as e:
        msg = "Could not parse PB table for name={}".format(name)
        if club is not None:
            msg += ', club={}'.format(club)
        msg += '\nException: {}'.format(e)
        raise ValueError(msg)
        
    header, *rows = table
    
    d = dict(rows)
    
    if 'Event' in d.keys():
        del d['Event']
    
    return url, d, s_link
        
        
def get_athelete_link(name, club=None):
    """ Get link to a given athlete's profile
    
        Paramters
        ---------
        name : str
            Athlete's full name. If more than two names are given, the first
            and last will be taken as 'Firstname Surname'; others will be 
            ignored. If only one name is given, it will used as Surname.
        club : str, optional
            Athlete's club
            
        Returns
        -------
        Full url of athlete's PowerOf10 profile and link to the search page
        from which it selected the athlete.
    """
    
    base = 'https://thepowerof10.info/athletes/'
    
    # check if name contains anything in brackets (like nee) and remove it
    m = re.search(r'\(.*\)', name)
    if m:
        i0, i1 = m.span()
        name = name[:i0] + name[i1:]
        
    # split 'name' into Firstname Surname (ideally)
    # if multiple names are given use the first and last
    # if only one name is given, use it as surname
    names = name.split(' ')
    # remove any empty list elements
    names = list(filter(None, names))
    # remove any trailing whitespace from the names
    names = [name.strip() for name in names]
    if len(names) == 1:
        firstname = ''
        surname = names[0]
    else:
        firstname = names[0]
        surname = names[-1]
    
    # make search url without club, even if it is provided
    url = _make_search_url(surname, firstname, None) 
    url = base+url
    
    # requested data
    req = [firstname, surname]
    if club is None:
        req.append('')
    else:
        req.append(club)
        
    # columns to get from search results table
    cols = ['First', 'Surname', 'Club', 'Profile']
    # for first three columns, get .text data, for last, get data in 'a' tag
    tags = [None, None, None, 'a']
    
    # get the required columns from the table
    try:
        table = _get_subtable(url, 'table', 'cphBody_dgAthletes', cols, 
                              tags=tags)
    except Exception:
        msg = "Could not parse search table for name={}".format(name)
        if club is not None:
            msg += ', club={}'.format(club)
        raise ValueError(msg)
        
    header, *rows = table
      
    # keep track of the maximum similarity
    max_sim = 0
    
    for row in rows:
        # find out how similar the results were
        l1, l2 = [], []
        for k in range(len(req)):
            l1 += _str_to_list(row[k].lower())
            l2 += _str_to_list(req[k].lower())
            
        similarity = cosine_similarity(l1, l2)
            
        # if this similarity is greater than the current max, get the link
        if similarity > max_sim:
            link = row[3][0].get('href')
            # reset amx_sim to current similarity
            max_sim = similarity
    
    # if a link was not found, 'link=base+link' will raise an exception
    try:
        link = base+link
    except UnboundLocalError:
        msg = ('Could not find link in table for name={}, club={}.'
               .format(name, club))
        raise ValueError(msg)
    
    return link, url


def _get_subtable(url, tag_name, attr_id, cols, tags=None):
    """ From a given url, find the element with `tag_name` at attribute id
        `attr_id`.
        
        Return a subset of the table given by the column headings `cols`
        
        Paramters
        ---------
        url : str
            url of page to read
        tag_name : str
            Name of tag containing the table
        attr_id : str
            Find table with tag=`tag_name` and id=`attr_id`
        cols : list
            List of column headings to make the subtable
        tags: list, optional
            List (same size as cols) of tags to return from each column.
            If not provided, `.text` is returned for each
    """
    
    # get html
    page = requests.get(url)
    soup = bs4.BeautifulSoup(page.text, 'html.parser')
    
    # get table of search results
    table = soup.find(tag_name, attrs={'id':attr_id})
    
    # raise exception if the table could not be found
    if table is None:
        raise ValueError
    
    # get table header
    rows = table.find_all('tr')
    headers = rows[0].find_all('td')
    header = [ele.text.strip() for ele in headers]
    # rest of the table
    rows = rows[1:]
    
    # get indices of requested columns
    idx = [header.index(c) for c in cols]
    
    # 'tab' will be filled with the requested colunms
    tab = []
    # put columns headers in tab
    tab.append(cols)
    
    # if 'tags' were not provided, make a list of None
    if tags is None:
        tags = [None]*len(cols)

    # for each row in the html data, get the data from the requested columns    
    for row in rows:
        # get all data from this row
        r = row.find_all('td')
        # temporary variable to store the requested data
        new_row = []
        for n in range(len(cols)):
            # if a specific tag was not requested, get .text
            if tags[n] is None:
                new_row.append(r[idx[n]].text)
            # otherwise, get the tag
            else:
                new_row.append(r[idx[n]](tags[n]))
        tab.append(new_row)
    
    return tab


def _make_search_url(*args):
    """ Make second half of url with name and club search terms.
    
        Parameters
        ----------
        surname : str
            Athlete surname
        firstname : str
            Athlete first name
        club : str, optional
            Athlete's club
    
        Returns
        -------
        String that must be appended to the base of the url
    """
    
    # cast args as list, not tuple
    args = list(args)
    
    # if None is is args, replace with empty string
    while None in args:
        i = args.index(None)
        args[i] = ''
    
    # replace any spaces with +
    args = [urllib.parse.quote_plus(a) for a in args]
    
    search = 'athleteslookup.aspx?surname={}&firstname={}&club={}'

    # raise exception if < 2 or > 3 args given
    if len(args) < 2:
        msg = ("'make_search_url' takes at least two arguments, {} given"
               .format(len(args)))
        raise ValueError(msg)
    elif len(args) > 3:
        msg = ("'make_search_url' takes at most three arguments, {} given"
               .format(len(args)))
        raise ValueError(msg)
    # if 2 args given (i.e. just name), append empty string (club name)
    elif len(args) == 2:
        args = args.append('')
    
    # put name data into search url
    search = search.format(*args)
    
    return search


def _str_to_list(s):
    """ Split string into words, separated by commas, slashes, ampersands or
        spaces
    """
    l = re.split(r',|/|&| ', s)
    l = list(filter(None, l))
    l = [item.strip() for item in l]
    return l


def cosine_similarity(l1, l2):
    """ Calculate the cosine similarity between two lists.
        The closer the result is to 1, the more similar the lists.
    """
    terms = set(l1).union(l2)
    dotprod = sum(l1.count(k) * l2.count(k) for k in terms)
    magA = (sum(l1.count(k)**2 for k in terms)) ** (1/2)
    magB = (sum(l2.count(k)**2 for k in terms)) ** (1/2)
    return dotprod / (magA * magB)


if __name__ == '__main__':
    
#    names = {'Josh Kerr':None, 'Keziah Milligan':None, 'Amy Allcock':None,
#             'Emily Hosker-Thornhill':'St Marys / Aldershot, Farnham & District',
#             'Laura Gent':'St Marys / Aldershot, Farnham & District'}
#    
#    for name in names.keys():
#        club = names[name]
#        try:
#            url = get_athelete_link(name, club=club)
#            print('{}: {}'.format(name, url))
#        except ValueError:
#            print('Could not find {}'.format(name, club=club))
            
    
#    name = 'Chris Thompson' # 'Sam Baylis' # 'Marc Hobbs' # 
#    club = 'Aldershot and Farnham District' # None # 'Swansea' # 
    
    name = 'Jane Evans (nee Groves)'
    club = 'Knowle and Dorridge'
    
    pbs = get_athlete_PB(name, club)
    print(pbs)
    
    
